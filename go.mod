module code.gitea.io/tea

go 1.12

require (
	code.gitea.io/gitea-vet v0.2.0
	code.gitea.io/sdk/gitea v0.13.0
	github.com/AlecAivazis/survey/v2 v2.1.1
	github.com/adrg/xdg v0.2.1
	github.com/araddon/dateparse v0.0.0-20200409225146-d820a6159ab1
	github.com/charmbracelet/glamour v0.2.0
	github.com/cpuguy83/go-md2man/v2 v2.0.0 // indirect
	github.com/go-git/go-git/v5 v5.1.0
	github.com/hashicorp/go-version v1.2.1 // indirect
	github.com/muesli/termenv v0.7.2
	github.com/olekukonko/tablewriter v0.0.4
	github.com/skratchdot/open-golang v0.0.0-20200116055534-eef842397966
	github.com/stretchr/testify v1.5.1
	github.com/urfave/cli/v2 v2.2.0
	golang.org/x/crypto v0.0.0-20200709230013-948cd5f35899
	golang.org/x/net v0.0.0-20200707034311-ab3426394381 // indirect
	golang.org/x/sys v0.0.0-20200625212154-ddb9806d33ae // indirect
	golang.org/x/tools v0.0.0-20200721032237-77f530d86f9a // indirect
	gopkg.in/yaml.v2 v2.3.0
)
